﻿using System.Runtime.CompilerServices;

namespace DynCre.Utils {
    public abstract class ConfigBase {
        /// <summary>
        /// Magic method to get param by setting property name
        /// </summary>
        /// <param name="propertyName">Caller member name passed instead</param>
        /// <returns>Config settings</returns>
        protected static string _getProp([CallerMemberName] string propertyName = null) {
            return ConfigUtils.GetAppSettings(propertyName);
        }

        /// <summary>
        /// Magic method to get connection string value by property name
        /// </summary>
        /// <param name="propertyName">Caller member name passed instead</param>
        /// <returns>Config settings</returns>
        protected static string _getConnectionString([CallerMemberName] string propertyName = null) {
            return ConfigUtils.GetConnectionString(propertyName);
        }
    }
}