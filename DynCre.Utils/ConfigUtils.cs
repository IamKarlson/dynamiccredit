﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Configuration;

using NLog;

namespace DynCre.Utils {
    /// <summary>
    /// Special helper for configurations
    /// </summary>
    public static class ConfigUtils {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Exe config
        /// </summary>
        private static Configuration currentAppConfiguration {
            get {
                Configuration currentAppConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return currentAppConfig;
            }
        }

        private static ConnectionStringSettingsCollection currentConnectionStrings {
            get {
                ConnectionStringSettingsCollection connStringsDict;

                if(HttpContext.Current == null) { connStringsDict = currentAppConfiguration.ConnectionStrings.ConnectionStrings; }
                else { connStringsDict = ConfigurationManager.ConnectionStrings; }
                return connStringsDict;
            }
        }

        /// <summary>
        /// Application settings key value
        /// </summary>
        /// <param name="key">app settings key name</param>
        /// <returns>setting without modification</returns>
        public static string GetAppSettings(string key) {
            try {
                // Web.config app settings 
                var appSettings = WebConfigurationManager.AppSettings;
                var setting = appSettings[key];
                if(setting == null) { return ""; }

                return setting;
            }
            catch(Exception e) {
                log.Error(e);
                return "";
            }
        }

        /// <summary>
        /// Get connection string
        /// </summary>
        /// <param name="key">Name of the connection string</param>
        /// <returns>Connection string without any modification</returns>
        public static string GetConnectionString(string key) {
            try {
                ConnectionStringSettingsCollection connStringsDict = currentConnectionStrings;

                ConnectionStringSettings connectionString = connStringsDict[key];

                if(connectionString == null) { return ""; }
                return connectionString.ConnectionString;
            }
            catch(Exception e) {
                log.Error(e);
                return string.Empty;
            }
        }

        /// <summary>
        /// Application settings key value
        /// </summary>
        /// <param name="key">app settings key name</param>
        /// <returns>Setting with any modification</returns>
        public static string GetFolderAppSettings(string key) {
            try {
                var setting = currentAppConfiguration.AppSettings.Settings[key];
                string folder = setting.Value;
                if(string.IsNullOrWhiteSpace(folder)) { folder = AppDomain.CurrentDomain.BaseDirectory; }
                if(!Directory.Exists(folder)) { Directory.CreateDirectory(folder); }
                return folder;
            }
            catch(Exception e) {
                log.Error(e);
                return "";
            }
        }
    }
}