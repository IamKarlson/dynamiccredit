﻿using System;

namespace DynCre.Utils {
    public static class UtilsConvert {
        public static DateTime? ParseDateSafe(string value) {
            DateTime? date;
            if(!string.IsNullOrWhiteSpace(value)) { date = DateTime.Parse(value); }
            else { date = null; }
            return date;
        }
    }
}