﻿using System.IO;

namespace DynCre.Utils {
    public static class UtilsIo {
        public static void CreateIfNotExist(string path) {
            if(!Directory.Exists(path)) { Directory.CreateDirectory(path); }
        }
    }
}