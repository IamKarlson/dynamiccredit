﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using NLog;

namespace DynCre.Utils {
    public class UniversalSerializator {
        private static readonly Logger _log = LogManager.GetCurrentClassLogger();

        public static bool SaveObjectToFile(object objectSaving, string path, bool indent = true, string root = "") {
            _log.Trace($"Saving object: {objectSaving.GetType().Name} to file {path}");
            var xml = SaveObjectToString(objectSaving, indent, root);
            File.WriteAllText(path, xml);
            return true;
        }

        public static string SaveObjectToString(object objectSaving, bool indent = true, string root = "") {
            _log.Trace($"Saving object: {objectSaving.GetType().Name}");
            var objtype = objectSaving.GetType();
            XmlSerializer serializer;
            if(string.IsNullOrWhiteSpace(root)) { serializer = new XmlSerializer(objtype); }
            else { serializer = new XmlSerializer(objtype, new XmlRootAttribute(root)); }
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            try {
                var sb = new StringBuilder();
                var writer = new StringWriter(sb);
                XmlWriter xmlwriter = XmlWriter.Create(writer,
                                                       new XmlWriterSettings() {
                                                           Indent = indent,
                                                           OmitXmlDeclaration = true
                                                       });
                serializer.Serialize(xmlwriter, objectSaving, ns);
                _log.Info($"Object ({objtype.Name}) saved");
                return sb.ToString();
            }
            catch(Exception ex) {
                _log.Error(ex, $"Saving object ({objtype.Name}) error");
                return string.Empty;
            }
        }

        public static T GetObject<T>(string path) {
            _log.Trace($"Get object from file {path}");
            var xml = File.ReadAllText(path);
            return GetObjectFromString<T>(xml);
        }

        public static T GetObjectFromString<T>(string objectString, string root = "") {
            Type objtype = typeof(T);
            XmlSerializer serializer;
            if(string.IsNullOrWhiteSpace(root)) { serializer = new XmlSerializer(objtype); }
            else { serializer = new XmlSerializer(objtype, new XmlRootAttribute(root)); }
            try {
                _log.Trace($"Object from string: type: {objtype.Name}");
                if(string.IsNullOrWhiteSpace(objectString)) { return default(T); }
                StringReader reader = new StringReader(objectString);
                return (T) serializer.Deserialize(reader);
            }
            catch(Exception ex) {
                _log.Error(ex, $"Getting object ({objtype.Name}) error");
                return default(T);
            }
        }
    }
}