﻿using DynCre.Services.Config;
using DynCre.Utils;

namespace DynCre {
    public class Config: ConfigBase, IFileSearcherConfiguration, IStorageLoaderConfiguration {
        public string FilesDir => _getProp();

        public string SearchPattern => _getProp();

        public string ArchiveFolder => _getProp();
    }
}