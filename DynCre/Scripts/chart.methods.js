﻿function buildChart() {

    $.ajax({

        type: "GET",
        url: "/Data/ChartData",
        contentType: "application/json; charset=utf-8", success: function (response) {
            const data = eval(response);
            var ctx = document.getElementById('reportChart').getContext('2d');

            var chart = new Chart(ctx, {
                type: 'bar',
                data: data.chartData,
                options: {
                    scales: data.options.scales,
                    tooltips: {
                        custom: function (tooltip) {
                            if (!tooltip) return;
                            // disable displaying the color box;
                            tooltip.displayColors = false;
                        },
                        callbacks: {
                            label: function (tooltipItems, currentData) {
                                const year = tooltipItems.xLabel;
                                const text = $.grep(data.tooltips, function (e) { return e.year == year; });
                                return text[0].text;
                            }
                        }
                    }
                }
            });

        }
    });
}
