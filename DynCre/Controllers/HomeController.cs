﻿using System.Threading.Tasks;
using System.Web.Mvc;

using DynCre.Dal;
using DynCre.Services;

namespace DynCre.Controllers {
    public class HomeController: Controller {
        private readonly IDataRepository dataRep;
        private readonly ITestDataGenerator generator;

        public HomeController(ITestDataGenerator generator, IDataRepository dataRep) {
            this.generator = generator;
            this.dataRep = dataRep;
        }

        public ActionResult Index() {
            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<ActionResult> Generate() {
            var result = await generator.ScaleData(3);
            // to scale it up to 4kk uncomment following line
            //var result = await generator.ScaleData(4000);
            if(!result) { return View("Error"); }
            return View("Index");
        }

        public async Task<ActionResult> Prepare() {
            bool result = await dataRep.PrepareData();
            if(!result) { return View("Error"); }
            return View("Index");
        }
    }
}