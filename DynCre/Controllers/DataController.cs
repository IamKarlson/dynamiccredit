﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

using DynCre.Dal;
using DynCre.Models.DTO;
using DynCre.Utils;

namespace DynCre.Controllers {
    public class DataController: Controller {
        private const string Y_AXIS_ORGVAL = "y-axis-orgVal";
        const string Y_AXIS_IDX = "y-axis-idx";
        private readonly IDataRepository dataRep;

        public DataController(IDataRepository dataRep) {
            this.dataRep = dataRep;
        }

        // GET: Data
        public async Task<ActionResult> Index() {
            ViewBag.Title = "Report page";
            var list = await dataRep.GetAggregatedDataForContractsByCurrentPrincipal();
            return View(list);
        }

        [HttpGet]
        public async Task<JsonResult> ChartData() {
            List<AggregatedChartDataDto> list = await dataRep.GetAggregatedChartData();

            var dtosSorted = list?.OrderBy(t => t.LoanOriginationYear).ThenBy(t => t.SortId).ToList();

            if(!dtosSorted?.Any() ?? true) { return null; }

            List<string> years = dtosSorted.Select(t => $"{t.LoanOriginationYear} - {t.RangeName}").ToList();

            var origVals = dtosSorted.Select(t => t.OriginalPropertyValue).ToList();
            var idxs = dtosSorted.Select(t => t.IndexedLTFV).ToList();

            var texts = dtosSorted.Select(t => new {
                year = $"{t.LoanOriginationYear} - {t.RangeName}",
                text = new List<string> {
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.OriginalPrincipalBalance)}: {t.OriginalPrincipalBalance}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.DebtToIncome)}: {t.DebtToIncome}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.LoanToIncome)}: {t.LoanToIncome}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.TotalIncome)}: {t.TotalIncome}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.IndexedDebtToIncome)}: {t.IndexedDebtToIncome}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.CurrentInterestRate)}: {t.CurrentInterestRate}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.OriginalLtv)}: {t.OriginalLtv}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.OriginalLtfv)}: {t.OriginalLtfv}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.OriginalForeclosureValue)}: {t.OriginalForeclosureValue}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.IndexedLTFV)}: {t.IndexedLTFV}",
                    $"{ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(i => i.OriginalPropertyValue)}: {t.OriginalPropertyValue}",
                }
            }).ToList();

            dynamic data = new {
                chartData = new {
                    labels = years,
                    datasets = new List<dynamic> {
                        new {
                            label = ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(t => t.OriginalPropertyValue),
                            data = origVals,
                            backgroundColor = "#f00",
                            yAxisID = Y_AXIS_ORGVAL,
                        },
                        new {
                            label = ReflectionUtils.GetPropertyDisplayName<AggregatedChartDataDto>(t => t.IndexedLTFV),
                            data = idxs,
                            backgroundColor = "#00f",
                            yAxisID = Y_AXIS_IDX
                        }
                    }
                },
                options = new {
                    scales = new {
                        yAxes = new List<dynamic>() {
                            new {
                                position = "left",
                                id = Y_AXIS_ORGVAL
                            },
                            new {
                                position = "right",
                                id = Y_AXIS_IDX
                            }
                        }
                    }
                },
                tooltips = texts
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}