﻿using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using DynCre.Dal;
using DynCre.Jobs;
using DynCre.Services;
using DynCre.Services.Config;

using Quartz;
using Quartz.Impl;

using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace DynCre {
    public class MvcApplication: HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            #region di
            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            var config = new Config();
            container.Register<IFileSearcherConfiguration>((() => config));
            container.Register<IStorageLoaderConfiguration>((() => config));
            container.Register<IFileSearcher, FileSearcher>();
            container.Register<IStorageLoader, DbLoader>();
            container.Register<IDataRepository, DataRepository>();

            container.Register<ITestDataGenerator, TestDataGenerator>();

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            #endregion di

            #region scheduler
            var sched = new StdSchedulerFactory().GetScheduler();
            sched.JobFactory = new InjectedJobFactory(container);
            sched.ScheduleJob(JobBuilder.Create<CrawlerJob>().Build(),
                              TriggerBuilder.Create().WithSimpleSchedule(s => s.WithIntervalInMinutes(1).RepeatForever()).Build());

            // start scheduler
            sched.Start();
            #endregion scheduler
        }
    }
}