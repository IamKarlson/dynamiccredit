﻿using System.Threading.Tasks;

namespace DynCre.Services {
    public interface ITestDataGenerator {
        Task<bool> ScaleData(int multiplier);
    }
}