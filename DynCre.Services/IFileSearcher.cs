﻿using System.Collections.Generic;
using System.IO;

namespace DynCre.Services {
    public interface IFileSearcher {
        IList<FileInfo> GetFiles();
    }
}