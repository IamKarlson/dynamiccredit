﻿namespace DynCre.Services.Config {
    public interface IFileSearcherConfiguration {
        string FilesDir { get; }

        string SearchPattern { get; }
    }
}