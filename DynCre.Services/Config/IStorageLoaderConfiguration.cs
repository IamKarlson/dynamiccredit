﻿namespace DynCre.Services.Config {
    public interface IStorageLoaderConfiguration {
        string ArchiveFolder { get; }
    }
}