﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

using DynCre.Services.Config;

namespace DynCre.Services {
    public class FileSearcher: IFileSearcher {
        private readonly IFileSearcherConfiguration config;

        public FileSearcher(IFileSearcherConfiguration config) {
            this.config = config;
        }

        public IList<FileInfo> GetFiles() {
            var dirPath = config.FilesDir;
            DirectoryInfo dir = new DirectoryInfo(dirPath);
            var searchPattern = config.SearchPattern;
            return dir.EnumerateFiles(searchPattern).ToList();
        }
    }
}