﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DynCre.Services {
    public interface IStorageLoader {
        Task<bool> LoadFiles(IList<FileInfo> file);
    }
}