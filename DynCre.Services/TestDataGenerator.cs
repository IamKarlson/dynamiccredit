﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

using DynCre.Models;

using Nelibur.ObjectMapper;

using NLog;

namespace DynCre.Services {
    public class TestDataGenerator: ITestDataGenerator {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        private readonly DynDbContext db;

        public TestDataGenerator() {
            db = new DynDbContext();
        }

        public async Task<bool> ScaleData(int multiplier) {
            try {
                var contracts = await db.Contracts.Include(i => i.Income).Include(m => m.Mortgage).Include(r => r.Rate)
                                        .Include(r => r.Securitization).Include(r => r.Valuation).Include(r => r.History).ToListAsync();

                var rand = new Random(100);
                ConcurrentBag<Contract> bag = new ConcurrentBag<Contract>();
                Parallel.For(0,
                             multiplier,
                             a => {
                                 decimal coef = new decimal((rand.Next(0, 100) + 50) / 100d);
                                 log.Trace($"Duplicating  with coef:{coef}");
                                 Parallel.ForEach(contracts, c => { bag.Add(duplicateWithCoef(c, coef)); });
                             });
                db.Contracts.AddRange(bag);
                await db.SaveChangesAsync();
            }
            catch(Exception ex) {
                log.Error(ex, "Error occurred");
                return false;
            }
            return true;
        }

        private static Contract duplicateWithCoef(Contract contractOld, decimal coef) {
            //log.Trace($"Duplicating of {contractOld.Id} with coef:{coef}");
            var contractNew = new Contract();
            var newMLoanId = Guid.NewGuid();
            var newMBorrowerId = Guid.NewGuid();
            var incomeNew = TinyMapper.Map<Income>(contractOld.Income);
            incomeNew.BorrowerTotalIncome *= coef;
            incomeNew.CurrentInterestShock *= coef;
            incomeNew.DebtToIncome *= coef;
            incomeNew.IndexedDebtToIncome *= coef;
            incomeNew.IndexedLoanToIncome *= coef;
            incomeNew.IndexedTotalIncome *= coef;
            incomeNew.LivingExpenses *= coef;
            incomeNew.LoanToIncome *= coef;
            incomeNew.MaxBorrowerIncome *= coef;
            incomeNew.MonthlyIncomeBuffer *= coef;
            incomeNew.MonthlyMortgageInterest *= coef;
            incomeNew.MonthlyMortgagePrincipal *= coef;
            incomeNew.TotalIncome *= coef;
            incomeNew.MLoanId = newMLoanId;
            contractNew.Income = incomeNew;

            var mortgageNew = TinyMapper.Map<Mortgage>(contractOld.Mortgage);
            mortgageNew.MLoanId = newMLoanId;
            contractNew.Mortgage = mortgageNew;

            var rateNew = TinyMapper.Map<Rate>(contractOld.Rate);
            rateNew.MLoanId = newMLoanId;
            contractNew.Rate = rateNew;

            var securitizationNew = TinyMapper.Map<Securitization>(contractOld.Securitization);
            securitizationNew.LifetimeBasePd *= coef;
            securitizationNew.LifetimeBasePdOverride *= coef;
            securitizationNew.LifetimeBasePpOverride *= coef;
            securitizationNew.MLoanId = newMLoanId;
            contractNew.Securitization = securitizationNew;

            var valuationNew = TinyMapper.Map<Valuation>(contractOld.Valuation);
            valuationNew.CurrentForeclosureValue *= coef;
            valuationNew.CurrentNetPropertyValue *= coef;
            valuationNew.CurrentPropertyValue *= coef;
            valuationNew.ForeclosureResidualDebtEstimate *= coef;
            valuationNew.IndexedLtfv *= coef;
            valuationNew.LoanLossEstimate *= coef;
            valuationNew.LoanPrice *= coef;
            valuationNew.OriginalForeclosureValue *= coef;
            valuationNew.OriginalLtfv *= coef;
            valuationNew.OriginalLtv *= coef;
            valuationNew.OriginalPropertyValue *= coef;
            valuationNew.MLoanId = newMLoanId;
            contractNew.Valuation = valuationNew;

            TinyMapper.Bind<Contract, Contract>(c => { c.Ignore(t => t.History); });
            contractNew.OriginalPrincipalBalance *= coef;
            contractNew.MLoanId = newMLoanId;
            contractNew.MBorrowerId = newMBorrowerId;

            contractNew.TermToMaturity = contractOld.TermToMaturity;
            contractNew.Guarantee = contractOld.Guarantee;
            contractNew.LoanTypeIndicator = contractOld.LoanTypeIndicator;
            contractNew.Sector = contractOld.Sector;
            contractNew.MaturityDate = contractOld.MaturityDate;
            contractNew.LoanOriginationDate = contractOld.LoanOriginationDate;

            contractNew.History = new List<HistoryElement>();

            foreach(HistoryElement h in contractOld.History) {
                HistoryElement newEl;
                if(typeof(History) == h.GetType()) {
                    var history = TinyMapper.Map<History>(h);
                    history.BorrowerExposure *= coef;
                    history.CurrentIndexedLtv *= coef;
                    history.CurrentLtv *= coef;
                    history.CurrentPrincipalBalance *= coef;
                    history.CurrentPropertyVal *= coef;
                    history.NoBorrowers *= coef;
                    history.Savings *= coef;
                    newEl = history;
                }
                else if(h.GetType() == typeof(HistoryRate)) {
                    var historyRate = TinyMapper.Map<HistoryRate>(h);
                    historyRate.CurrentDebtToIncome *= coef;
                    historyRate.CurrentInterestRate *= coef;
                    historyRate.CurrentLtfv *= coef;
                    newEl = historyRate;
                }
                else { throw new ArgumentOutOfRangeException(nameof(contractNew.History), "Unsupported type"); }
                newEl.MLoanId = newMLoanId;
            }

            return contractNew;
        }
    }
}