﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using DynCre.Models;
using DynCre.Models.Serialization;
using DynCre.Services.Config;
using DynCre.Utils;

using NLog;

namespace DynCre.Services {
    public class DbLoader: IStorageLoader {
        private readonly IStorageLoaderConfiguration config;
        private readonly DynDbContext db;
        private readonly Logger log = LogManager.GetCurrentClassLogger();

        public DbLoader(IStorageLoaderConfiguration config) {
            this.config = config;
            db = new DynDbContext();
        }

        public async Task<bool> LoadFiles(IList<FileInfo> files) {
            log.Trace("loading file");

            try {
                List<FileInfo> filesToMove = new List<FileInfo>();
                UtilsIo.CreateIfNotExist(config.ArchiveFolder);
                for(var i = 0; i < files.Count; i++) {
                    FileInfo file = files[i];
                    if(file == null) { throw new ArgumentNullException(nameof(file)); }
                    try {
                        if(File.Exists(file?.FullName)) {
                            log.Info($"Loading {file.Name} to the database (from folder {file?.Directory?.FullName})");
                            var contractLoad = UniversalSerializator.GetObject<ContractLoad>(file.FullName);
                            if(contractLoad.Unsupported?.Length > 0) { throw new ArgumentException("File contains unsupported elements"); }
                            Contract entry = contractLoad.Contract;
                            entry.OriginSource = file.Name;
                            db.Contracts.Add(entry);
                            log.Debug($"File is loaded from {file.FullName} succesfully.");
                            filesToMove.Add(file);
                        }

                        if((i + 1) % 500 == 0) {
                            Parallel.ForEach(filesToMove.Select(t => new {
                                                 t.FullName,
                                                 t.Name
                                             }).ToList(),
                                             (fileToMove) => {
                                                 if(File.Exists(fileToMove.FullName)) {
                                                     File.Move(fileToMove.FullName, Path.Combine(config.ArchiveFolder, fileToMove.Name));
                                                 }
                                             });
                            filesToMove.Clear();
                            await db.SaveChangesAsync();
                        }
                    }
                    catch(Exception ex) {
                        log.Error(ex, "Error durring file loading");
                        log.Debug($"File is not loaded from {file.FullName}. Reasons are mentioned above in the log.");
                    }
                }
                Parallel.ForEach(filesToMove,
                                 (fileToMove) => {
                                     if(File.Exists(fileToMove.FullName)) { }
                                 });
                filesToMove.Clear();
                await db.SaveChangesAsync();
                return true;
            }
            catch(Exception ex) {
                log.Error(ex, "Cannot perform load");
                return false;
            }
        }
    }
}