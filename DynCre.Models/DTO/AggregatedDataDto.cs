﻿using System.ComponentModel.DataAnnotations;

namespace DynCre.Models.DTO {
    public class AggregatedDataDto {
        [Display(Name = "Range")]
        public string RangeName { get; set; }

        public int SortId { get; set; }

        [Display(Name = "Total rows")]
        public int TotalRecordsCount { get; set; }

        [Display(Name = "Original Principal Balance")]
        public decimal? OriginalPrincipalBalance { get; set; }

        [Display(Name = "Debt To Income")]
        public decimal? DebtToIncome { get; set; }

        [Display(Name = "Loan To Income")]
        public decimal? LoanToIncome { get; set; }

        [Display(Name = "Total Income")]
        public decimal? TotalIncome { get; set; }

        [Display(Name = "Indexed Debt To Income")]
        public decimal? IndexedDebtToIncome { get; set; }

        [Display(Name = "Indexed Loan To Income")]
        public decimal? IndexedLoanToIncome { get; set; }

        [Display(Name = "Current Interest Rate")]
        public decimal? CurrentInterestRate { get; set; }

        [Display(Name = "Original Ltv")]
        public decimal? OriginalLtv { get; set; }

        [Display(Name = "Original Ltfv")]
        public decimal? OriginalLtfv { get; set; }

        [Display(Name = "Original Foreclosure Value")]
        public decimal? OriginalForeclosureValue { get; set; }

        [Display(Name = "Indexed LTFV")]
        public decimal? IndexedLTFV { get; set; }
    }
}