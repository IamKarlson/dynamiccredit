﻿namespace DynCre.Models {
    public class Constants {
        public const string PROCEDURE_SELECT_AGGREGATED_CONTRACTS_WITH_YEAR = "GetAggregatedDataWithYear";
        public const string PROCEDURE_SET_UP_BUCKETS = "SetUpBuckets";
        public const string PROCEDURE_SELECT_AGGREGATED_CONTRACTS = "GetAggregatedData";
        public const string PROCEDURE_AGGREGATE_CONTRACTS = "AggregateContracts";

        public const string TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL = "AggregatedContractsByCurrentPrincipal";
    }
}