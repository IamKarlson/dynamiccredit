﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml;
using System.Xml.Serialization;

using DynCre.Utils;

namespace DynCre.Models {
    [XmlRoot(ElementName = "MASTER")]
    public class Contract: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlIgnore]
        public string OriginSource { get; set; }

        [XmlIgnore]
        public bool IsAggergated { get; set; }

        [XmlElement(ElementName = "M_BorrowerID")]
        public Guid MBorrowerId { get; set; }

        [XmlElement(ElementName = "ClassType")]
        public int ClassType { get; set; }

        [XmlElement(ElementName = "OriginalPrincipalBalance")]
        public decimal? OriginalPrincipalBalance { get; set; }

        [XmlElement(ElementName = "LoanOriginationDate")]
        [NotMapped]
        public virtual string LoanOriginationDateProxy {
            get => LoanOriginationDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set => LoanOriginationDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? LoanOriginationDate { get; set; }

        [XmlElement(ElementName = "MaturityDate")]
        [NotMapped]
        public virtual string MaturityDateProxy {
            get => MaturityDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set => MaturityDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? MaturityDate { get; set; }

        [XmlElement(ElementName = "TermToMaturity")]
        public int TermToMaturity { get; set; }

        [XmlElement(ElementName = "Guarantee")]
        public int Guarantee { get; set; }

        [XmlElement(ElementName = "LoanTypeIndicator")]
        public int LoanTypeIndicator { get; set; }

        [XmlElement(ElementName = "Sector")]
        public string Sector { get; set; }

        [XmlElement(ElementName = "INCOME")]
        public virtual Income Income { get; set; }

        [XmlElement(ElementName = "MORTGAGE")]
        public virtual Mortgage Mortgage { get; set; }

        [XmlElement(ElementName = "RATES")]
        public virtual Rate Rate { get; set; }

        [XmlElement(ElementName = "SECURITIZATION")]
        public virtual Securitization Securitization { get; set; }

        [XmlElement(ElementName = "VALUATION")]
        public virtual Valuation Valuation { get; set; }

        [XmlArray(ElementName = "HISTORY")]
        [XmlArrayItem(ElementName = "HISTORY", Type = typeof(History))]
        [XmlArrayItem(ElementName = "HISTORY_RATE", Type = typeof(HistoryRate))]
        public virtual List<HistoryElement> History { get; set; }

        [XmlAnyElement]
        public XmlElement[] Unsupported { get; set; }
    }
}