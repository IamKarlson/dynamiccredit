﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

using DynCre.Utils;

namespace DynCre.Models {
    [XmlRoot(ElementName = "RATES")]
    public class Rate: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlElement(ElementName = "ReversionaryDate")]
        [NotMapped]
        public virtual string ReversionaryDateProxy {
            get => ReversionaryDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set { ReversionaryDate = UtilsConvert.ParseDateSafe(value); }
        }

        [XmlIgnore]
        public DateTime? ReversionaryDate { get; set; }

        [XmlElement(ElementName = "InterestStartDate")]
        [NotMapped]
        public virtual string InterestStartDateProxy {
            get => InterestStartDate?.ToString("yyyy-MM-dd");
            set => InterestStartDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? InterestStartDate { get; set; }

        [XmlElement(ElementName = "RemainingFixedRateTerm")]
        public string RemainingFixedRateTerm { get; set; }

        [XmlElement(ElementName = "PrincipalStartDate")]
        [NotMapped]
        public virtual string PrincipalStartDateProxy {
            get => PrincipalStartDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set => PrincipalStartDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? PrincipalStartDate { get; set; }

        [XmlElement(ElementName = "OriginalFixedRateTerm")]
        public int OriginalFixedRateTerm { get; set; }
    }
}