﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace DynCre.Models {
    [XmlRoot(ElementName = "MORTGAGE")]
    public class Mortgage: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlElement(ElementName = "BTLStatus")]
        public int BuyToLetStatus { get; set; }

        [XmlElement(ElementName = "RepaymentType")]
        public string RepaymentType { get; set; }

        [XmlElement(ElementName = "SecondLien")]
        public int SecondLien { get; set; }
    }
}