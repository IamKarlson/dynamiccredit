using System.Data.Entity;

using NLog;

namespace DynCre.Models {
    public class DynDbContext: DbContext {
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }

        public virtual DbSet<History> HistoryElements { get; set; }

        public virtual DbSet<HistoryRate> HistoryRates { get; set; }

        public virtual DbSet<Income> Incomes { get; set; }

        public virtual DbSet<Mortgage> Mortgages { get; set; }

        public virtual DbSet<Rate> Rates { get; set; }

        public virtual DbSet<Securitization> Securitizations { get; set; }

        public virtual DbSet<Valuation> Valuations { get; set; }

        public virtual DbSet<Range> Ranges { get; set; }

        private readonly Logger log = LogManager.GetCurrentClassLogger();

        // Your context has been configured to use a 'DbContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'DynCre.Models.DbContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DbContext' 
        // connection string in the application configuration file.
        public DynDbContext(): base("name=DynDbContext") {
            log.Trace("ctor");
        }
    }
}