﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Xml.Serialization;

namespace DynCre.Models {
    [Table(nameof(HistoryRate))]
    [XmlRoot(ElementName = "HISTORY_RATE")]
    public class HistoryRate: HistoryElement {
        [XmlElement(ElementName = "CurrentRateType")]
        public string CurrentRateType { get; set; }

        [XmlElement(ElementName = "CurrentInterestRate")]
        [NotMapped]
        public virtual string CurrentInterestRateProxy {
            get => CurrentInterestRate?.ToString("F");
            set => CurrentInterestRate = decimal.Parse(value, NumberStyles.Currency);
        }

        [XmlIgnore]
        public decimal? CurrentInterestRate { get; set; }

        [XmlElement(ElementName = "CurrentDTI")]
        public decimal? CurrentDebtToIncome { get; set; }

        [XmlElement(ElementName = "CurrentLTFV")]
        public decimal? CurrentLtfv { get; set; }
    }
}