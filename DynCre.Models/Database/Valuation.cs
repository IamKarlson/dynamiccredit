﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

using DynCre.Utils;

namespace DynCre.Models {
    [XmlRoot(ElementName = "VALUATION")]
    public class Valuation: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlElement(ElementName = "OriginalPropertyValue")]
        public decimal? OriginalPropertyValue { get; set; }

        [XmlElement(ElementName = "PropertyValuationDate")]
        [NotMapped]
        public virtual string PropertyValuationDateProxy {
            get => PropertyValuationDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set => PropertyValuationDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? PropertyValuationDate { get; set; }

        [XmlElement(ElementName = "OriginalLTV")]
        public decimal? OriginalLtv { get; set; }

        [XmlElement(ElementName = "OriginalLTFV")]
        public decimal? OriginalLtfv { get; set; }

        [XmlElement(ElementName = "OriginalForeclosureValue")]
        public decimal? OriginalForeclosureValue { get; set; }

        [XmlElement(ElementName = "IndexedLTFV")]
        public decimal? IndexedLtfv { get; set; }

        [XmlElement(ElementName = "CurrentForeclosureValue")]
        public decimal? CurrentForeclosureValue { get; set; }

        [XmlElement(ElementName = "CurrentPropertyValue")]
        public decimal? CurrentPropertyValue { get; set; }

        [XmlElement(ElementName = "CurrentNetPropertyValue")]
        public decimal? CurrentNetPropertyValue { get; set; }

        [XmlElement(ElementName = "ForeclosureResidualDebtEstimate")]
        public decimal? ForeclosureResidualDebtEstimate { get; set; }

        [XmlElement(ElementName = "LoanLossEstimate")]
        public decimal? LoanLossEstimate { get; set; }

        [XmlElement(ElementName = "LoanPrice")]
        public decimal? LoanPrice { get; set; }
    }
}