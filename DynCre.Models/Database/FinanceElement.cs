﻿using System;
using System.Xml.Serialization;

namespace DynCre.Models {
    public abstract class FinanceElement {
        [XmlElement(ElementName = "M_PoolID")]
        public string MPoolId { get; set; }

        [XmlElement(ElementName = "M_LoanID")]
        public virtual Guid MLoanId { get; set; }
    }
}