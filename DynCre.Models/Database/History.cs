﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace DynCre.Models {
    [Table(nameof(History))]
    [XmlRoot(ElementName = "HISTORY")]
    public class History: HistoryElement {
        [XmlElement(ElementName = "CurrentPrincipalBalance")]
        public decimal? CurrentPrincipalBalance { get; set; }

        [XmlElement(ElementName = "CurrentLTV")]
        public decimal? CurrentLtv { get; set; }

        [XmlElement(ElementName = "LoanAge")]
        public int LoanAge { get; set; }

        [XmlElement(ElementName = "CurrentPropertyVal")]
        public decimal? CurrentPropertyVal { get; set; }

        [XmlElement(ElementName = "Savings")]
        public decimal? Savings { get; set; }

        [XmlElement(ElementName = "CurrentIndexedLTV")]
        public decimal? CurrentIndexedLtv { get; set; }

        [XmlElement(ElementName = "DelinquencyStatus")]

        public string DelinquencyStatus { get; set; }

        [XmlElement(ElementName = "BorrowerExposure")]
        public decimal? BorrowerExposure { get; set; }

        [XmlElement(ElementName = "NoBorrowers")]
        public decimal? NoBorrowers { get; set; }
    }
}