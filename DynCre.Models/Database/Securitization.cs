﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace DynCre.Models {
    [XmlRoot(ElementName = "SECURITIZATION")]
    public class Securitization: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlElement(ElementName = "LifetimeBasePD")]
        public decimal? LifetimeBasePd { get; set; }

        [XmlElement(ElementName = "LifetimeBasePDOverride")]
        public decimal? LifetimeBasePdOverride { get; set; }

        [XmlElement(ElementName = "Multiple_NEW")]
        public decimal? MultipleNew { get; set; }

        [XmlElement(ElementName = "LifetimeBasePPOverride")]
        public decimal? LifetimeBasePpOverride { get; set; }
    }
}