﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

using DynCre.Utils;

namespace DynCre.Models {
    [XmlRoot(ElementName = "INCOME")]
    public class Income: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlElement(ElementName = "DTI")]
        public decimal? DebtToIncome { get; set; }

        [XmlElement(ElementName = "LTI")]
        public decimal? LoanToIncome { get; set; }

        [XmlElement(ElementName = "SelfCertification")]
        public string SelfCertification { get; set; }

        [XmlElement(ElementName = "EmploymentStatus")]
        public string EmploymentStatus { get; set; }

        [XmlElement(ElementName = "TotalIncome")]
        public decimal? TotalIncome { get; set; }

        [XmlElement(ElementName = "BorrowerBirthDate")]
        [NotMapped]
        public virtual string BorrowerBirthDateProxy {
            get => BorrowerBirthDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set => BorrowerBirthDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? BorrowerBirthDate { get; set; }

        [XmlElement(ElementName = "IndexedDTI")]
        public decimal? IndexedDebtToIncome { get; set; }

        [XmlElement(ElementName = "IndexedLTI")]
        public decimal? IndexedLoanToIncome { get; set; }

        [XmlElement(ElementName = "IndexedTotalIncome")]
        public decimal? IndexedTotalIncome { get; set; }

        [XmlElement(ElementName = "MaxBorrowerIncome")]
        public decimal? MaxBorrowerIncome { get; set; }

        [XmlElement(ElementName = "BorrowerTotalIncome")]
        public decimal? BorrowerTotalIncome { get; set; }

        [XmlElement(ElementName = "LivingExpenses")]
        public decimal? LivingExpenses { get; set; }

        [XmlElement(ElementName = "CurrentInterestShock")]
        public decimal? CurrentInterestShock { get; set; }

        [XmlElement(ElementName = "MonthlyNetIncome")]
        public string MonthlyNetIncome { get; set; }

        [XmlElement(ElementName = "MonthlyMortgageInterest")]
        public decimal? MonthlyMortgageInterest { get; set; }

        [XmlElement(ElementName = "MonthlyMortgagePrincipal")]
        public decimal? MonthlyMortgagePrincipal { get; set; }

        [XmlElement(ElementName = "MonthlyIncomeBuffer")]
        public decimal? MonthlyIncomeBuffer { get; set; }
    }
}