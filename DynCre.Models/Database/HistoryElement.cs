﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

using DynCre.Utils;

namespace DynCre.Models {
    [Serializable]
    public abstract class HistoryElement: FinanceElement {
        [XmlIgnore]
        [Key]
        public int Id { get; set; }

        [XmlElement(ElementName = "ReportDate")]
        [NotMapped]
        public virtual string ReportDateProxy {
            get => ReportDate?.ToString("yyyy-MM-dd HH:mm:ss");
            set => ReportDate = UtilsConvert.ParseDateSafe(value);
        }

        [XmlIgnore]
        public DateTime? ReportDate { get; set; }
    }
}