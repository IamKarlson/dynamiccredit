using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class ReportDateType: DbMigration {
        public override void Up() {
            AlterColumn("dbo.HistoryElements", "ReportDate", c => c.DateTime());
        }

        public override void Down() {
            AlterColumn("dbo.HistoryElements", "ReportDate", c => c.String());
        }
    }
}