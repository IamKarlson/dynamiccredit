using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class UpdatedProcForChart: DbMigration {
        public override void Up() {
            DropStoredProcedure(Constants.PROCEDURE_AGGREGATE_CONTRACTS);
            CreateStoredProcedure(Constants.PROCEDURE_AGGREGATE_CONTRACTS,
                                  body: $@"
BEGIN	
    IF(OBJECT_ID('{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}') IS NOT NULL)
        DROP TABLE [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}]

    UPDATE Contracts SET [IsAggergated] =0

    IF(OBJECT_ID('{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}') IS NULL)
        BEGIN
            CREATE TABLE [dbo].[{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}]
            (
                 [ContractId]               [INT] NOT NULL
                ,[CurrentPrincipalBalance]  [DECIMAL](23,5) NULL
                ,[OriginalPrincipalBalance] [DECIMAL](23,5) NULL
                ,[debttoincome]             [DECIMAL](23,5) NULL
                ,[loantoincome]             [DECIMAL](23,5) NULL
                ,[TotalIncome]              [DECIMAL](23,5) NULL
                ,[IndexedDebtToIncome]      [DECIMAL](23,5) NULL
                ,[IndexedLoanToIncome]      [DECIMAL](23,5) NULL
                ,[CurrentInterestRate]      [DECIMAL](23,5) NULL
                ,[OriginalLtv]              [DECIMAL](23,5) NULL
                ,[OriginalLtfv]             [DECIMAL](23,5) NULL
                ,[OriginalForeclosureValue] [DECIMAL](23,5) NULL
                ,[IndexedLTFV]              [DECIMAL](23,5) NULL
                ,[LoanOriginationDate]      [DATETIME] NULL
                ,[OriginalPropertyValue]    [DECIMAL](23,5) NULL  
            )
            ON [PRIMARY]
        END
    DECLARE @Unmerged INT
    SELECT
            @unmerged = COUNT(*)
    FROM [Contracts]
    WHERE [IsAggergated] = 0

    IF(@Unmerged = 0)
    BEGIN
        RETURN 0
    END


    BEGIN TRAN [TransformData]

    BEGIN TRY

        INSERT INTO [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}]
            SELECT
                [c].[Id] AS [ContractId]
                ,[h].[CurrentPrincipalBalance]
                ,[c].[OriginalPrincipalBalance]
                ,[i].[debttoincome]
                ,[i].[loantoincome]
                ,[i].[TotalIncome]
                ,[i].[IndexedDebtToIncome]
                ,[i].[IndexedLoanToIncome]
                ,[his].[CurrentInterestRate]
                ,[v].[OriginalLtv]
                ,[v].[OriginalLtfv]
                ,[v].[OriginalForeclosureValue]
                ,[v].[IndexedLTFV]
            FROM [dbo].[Contracts] AS [c]
            LEFT JOIN [dbo].[Incomes] AS [i]
                ON [i].[id] = [c].[Income_Id]
            LEFT JOIN
                (
                    SELECT
                        [he].[Contract_Id]
                        ,[he].[ReportDate]
                        ,[h].[CurrentPrincipalBalance]
                    FROM [dbo].[HistoryElements] AS [he]
                    INNER JOIN [dbo].[History] AS [h]
                        ON [he].[Id] = [h].[Id]
                ) AS [h]
                ON [c].[Id] = [h].[Contract_Id]
            LEFT JOIN
                (
                    SELECT
                        [he].[Contract_Id]
                        ,[he].[ReportDate]
                        ,[h].[CurrentInterestRate]
                    FROM [dbo].[HistoryElements] AS [he]
                    INNER JOIN [dbo].[HistoryRate] AS [h]
                        ON [he].[Id] = [h].[Id]
                ) AS [his]
                ON [c].[Id] = [his].[Contract_Id]
                    AND [his].[ReportDate] = [h].[ReportDate]
            LEFT JOIN [dbo].[Valuations] AS [v]
                ON [v].[id] = [c].[Valuation_Id]
            WHERE [h].[CurrentPrincipalBalance] IS NOT NULL
                AND [IsAggergated] = 0


        UPDATE [c]
            SET
                [IsAggergated] = 1
        FROM [dbo].[Contracts] AS [c]
                INNER JOIN [dbo].[{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}] AS [cp]
                    ON [cp].[ContractId] = [c].[id]
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN [TransformData]
        RETURN
    END CATCH

    COMMIT TRAN [TransformData]
END

");
        }

        public override void Down() {
        }
    }
}