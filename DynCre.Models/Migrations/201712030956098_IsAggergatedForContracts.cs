using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class IsAggergatedForContracts: DbMigration {
        public override void Up() {
            AddColumn("dbo.Contracts", "IsAggergated", c => c.Boolean(nullable: false));
        }

        public override void Down() {
            DropColumn("dbo.Contracts", "IsAggergated");
        }
    }
}