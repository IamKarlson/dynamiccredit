using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class EfRelationsFix: DbMigration {
        public override void Up() {
            CreateTable("dbo.History",
                        c => new {
                            Id = c.Int(nullable: false),
                            CurrentPrincipalBalance = c.Decimal(precision: 23, scale: 5),
                            CurrentLtv = c.Decimal(precision: 23, scale: 5),
                            LoanAge = c.Int(nullable: false),
                            CurrentPropertyVal = c.Decimal(precision: 23, scale: 5),
                            Savings = c.Decimal(precision: 23, scale: 5),
                            CurrentIndexedLtv = c.Decimal(precision: 23, scale: 5),
                            DelinquencyStatus = c.String(),
                            BorrowerExposure = c.Decimal(precision: 23, scale: 5),
                            NoBorrowers = c.Decimal(precision: 23, scale: 5),
                        }).PrimaryKey(t => t.Id).ForeignKey("dbo.HistoryElements", t => t.Id).Index(t => t.Id);

            CreateTable("dbo.HistoryRate",
                        c => new {
                            Id = c.Int(nullable: false),
                            CurrentRateType = c.String(),
                            CurrentInterestRate = c.Decimal(precision: 23, scale: 5),
                            CurrentDebtToIncome = c.Decimal(precision: 23, scale: 5),
                            CurrentLtfv = c.Decimal(precision: 23, scale: 5),
                        }).PrimaryKey(t => t.Id).ForeignKey("dbo.HistoryElements", t => t.Id).Index(t => t.Id);

            DropColumn("dbo.HistoryElements", "CurrentPrincipalBalance");
            DropColumn("dbo.HistoryElements", "CurrentLtv");
            DropColumn("dbo.HistoryElements", "LoanAge");
            DropColumn("dbo.HistoryElements", "CurrentPropertyVal");
            DropColumn("dbo.HistoryElements", "Savings");
            DropColumn("dbo.HistoryElements", "CurrentIndexedLtv");
            DropColumn("dbo.HistoryElements", "DelinquencyStatus");
            DropColumn("dbo.HistoryElements", "BorrowerExposure");
            DropColumn("dbo.HistoryElements", "NoBorrowers");
            DropColumn("dbo.HistoryElements", "CurrentRateType");
            DropColumn("dbo.HistoryElements", "CurrentInterestRate");
            DropColumn("dbo.HistoryElements", "CurrentDebtToIncome");
            DropColumn("dbo.HistoryElements", "CurrentLtfv");
            DropColumn("dbo.HistoryElements", "Discriminator");
        }

        public override void Down() {
            AddColumn("dbo.HistoryElements", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.HistoryElements", "CurrentLtfv", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "CurrentDebtToIncome", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "CurrentInterestRate", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "CurrentRateType", c => c.String());
            AddColumn("dbo.HistoryElements", "NoBorrowers", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "BorrowerExposure", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "DelinquencyStatus", c => c.String());
            AddColumn("dbo.HistoryElements", "CurrentIndexedLtv", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "Savings", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "CurrentPropertyVal", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "LoanAge", c => c.Int());
            AddColumn("dbo.HistoryElements", "CurrentLtv", c => c.Decimal(precision: 23, scale: 5));
            AddColumn("dbo.HistoryElements", "CurrentPrincipalBalance", c => c.Decimal(precision: 23, scale: 5));
            DropForeignKey("dbo.HistoryRate", "Id", "dbo.HistoryElements");
            DropForeignKey("dbo.History", "Id", "dbo.HistoryElements");
            DropIndex("dbo.HistoryRate",
                      new[] {
                          "Id"
                      });
            DropIndex("dbo.History",
                      new[] {
                          "Id"
                      });
            DropTable("dbo.HistoryRate");
            DropTable("dbo.History");
        }
    }
}