using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class SecuritizationDataTypes: DbMigration {
        public override void Up() {
            AlterColumn("dbo.Securitizations", "LifetimeBasePd", c => c.Decimal(precision: 23, scale: 5));
            AlterColumn("dbo.Securitizations", "LifetimeBasePdOverride", c => c.Decimal(precision: 23, scale: 5));
            AlterColumn("dbo.Securitizations", "MultipleNew", c => c.Decimal(precision: 23, scale: 5));
            AlterColumn("dbo.Securitizations", "LifetimeBasePpOverride", c => c.Decimal(precision: 23, scale: 5));
        }

        public override void Down() {
            AlterColumn("dbo.Securitizations", "LifetimeBasePpOverride", c => c.String());
            AlterColumn("dbo.Securitizations", "MultipleNew", c => c.String());
            AlterColumn("dbo.Securitizations", "LifetimeBasePdOverride", c => c.String());
            AlterColumn("dbo.Securitizations", "LifetimeBasePd", c => c.String());
        }
    }
}