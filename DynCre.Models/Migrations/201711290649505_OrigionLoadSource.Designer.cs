// <auto-generated />
namespace DynCre.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class OrigionLoadSource : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(OrigionLoadSource));
        
        string IMigrationMetadata.Id
        {
            get { return "201711290649505_OrigionLoadSource"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
