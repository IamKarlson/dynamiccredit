using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class RangeNullable: DbMigration {
        public override void Up() {
            AlterColumn("dbo.Ranges", "MinLevel", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Ranges", "MaxLevel", c => c.Decimal(precision: 18, scale: 2));
        }

        public override void Down() {
            AlterColumn("dbo.Ranges", "MaxLevel", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Ranges", "MinLevel", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}