using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class AddAggregateDataProcedure: DbMigration {
        public override void Up() {
            #region create
            CreateStoredProcedure(Constants.PROCEDURE_AGGREGATE_CONTRACTS,
                                  body: $@"BEGIN	
    IF(OBJECT_ID('{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}') IS NOT NULL)
        DROP TABLE [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}]

    UPDATE Contracts SET [IsAggergated] =0

    IF(OBJECT_ID('{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}') IS NULL)
        BEGIN
            CREATE TABLE [dbo].[{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}]
            (
                 [ContractId]               [INT] NOT NULL
                ,[CurrentPrincipalBalance]  [DECIMAL](23,5) NULL
                ,[OriginalPrincipalBalance] [DECIMAL](23,5) NULL
                ,[debttoincome]             [DECIMAL](23,5) NULL
                ,[loantoincome]             [DECIMAL](23,5) NULL
                ,[TotalIncome]              [DECIMAL](23,5) NULL
                ,[IndexedDebtToIncome]      [DECIMAL](23,5) NULL
                ,[IndexedLoanToIncome]      [DECIMAL](23,5) NULL
                ,[CurrentInterestRate]      [DECIMAL](23,5) NULL
                ,[OriginalLtv]              [DECIMAL](23,5) NULL
                ,[OriginalLtfv]             [DECIMAL](23,5) NULL
                ,[OriginalForeclosureValue] [DECIMAL](23,5) NULL
                ,[IndexedLTFV]              [DECIMAL](23,5) NULL
                ,[LoanOriginationDate]      [DATETIME] NULL
                ,[OriginalPropertyValue]    [DECIMAL](23,5) NULL  
            )
            ON [PRIMARY]
        END
    DECLARE @Unmerged INT
    SELECT
            @unmerged = COUNT(*)
    FROM [Contracts]
    WHERE [IsAggergated] = 0

    IF(@Unmerged = 0)
    BEGIN
        RETURN 0
    END


    BEGIN TRAN [TransformData]

    BEGIN TRY

        INSERT INTO [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}]
            SELECT
                [c].[Id] AS [ContractId]
                ,[h].[CurrentPrincipalBalance]
                ,[c].[OriginalPrincipalBalance]
                ,[i].[debttoincome]
                ,[i].[loantoincome]
                ,[i].[TotalIncome]
                ,[i].[IndexedDebtToIncome]
                ,[i].[IndexedLoanToIncome]
                ,[his].[CurrentInterestRate]
                ,[v].[OriginalLtv]
                ,[v].[OriginalLtfv]
                ,[v].[OriginalForeclosureValue]
                ,[v].[IndexedLTFV]
            FROM [dbo].[Contracts] AS [c]
            LEFT JOIN [dbo].[Incomes] AS [i]
                ON [i].[id] = [c].[Income_Id]
            LEFT JOIN
                (
                    SELECT
                        [he].[Contract_Id]
                        ,[he].[ReportDate]
                        ,[h].[CurrentPrincipalBalance]
                    FROM [dbo].[HistoryElements] AS [he]
                    INNER JOIN [dbo].[History] AS [h]
                        ON [he].[Id] = [h].[Id]
                ) AS [h]
                ON [c].[Id] = [h].[Contract_Id]
            LEFT JOIN
                (
                    SELECT
                        [he].[Contract_Id]
                        ,[he].[ReportDate]
                        ,[h].[CurrentInterestRate]
                    FROM [dbo].[HistoryElements] AS [he]
                    INNER JOIN [dbo].[HistoryRate] AS [h]
                        ON [he].[Id] = [h].[Id]
                ) AS [his]
                ON [c].[Id] = [his].[Contract_Id]
                    AND [his].[ReportDate] = [h].[ReportDate]
            LEFT JOIN [dbo].[Valuations] AS [v]
                ON [v].[id] = [c].[Valuation_Id]
            WHERE [h].[CurrentPrincipalBalance] IS NOT NULL
                AND [IsAggergated] = 0


        UPDATE [c]
            SET
                [IsAggergated] = 1
        FROM [dbo].[Contracts] AS [c]
                INNER JOIN [dbo].[{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}] AS [cp]
                    ON [cp].[ContractId] = [c].[id]
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN [TransformData]
        RETURN
    END CATCH

    COMMIT TRAN [TransformData]
END
");
            #endregion create

            #region select
            CreateStoredProcedure(Constants.PROCEDURE_SELECT_AGGREGATED_CONTRACTS,
                                  body: $@"
/* 
Used for showing data on the page
*/
    IF(OBJECT_ID('tempdb..#aggregated') IS NOT NULL)
        DROP TABLE [#aggregated]

    IF(OBJECT_ID('tempdb..#ranges') IS NOT NULL)
        DROP TABLE [#ranges]

    BEGIN

/*
Generating ranges for buckets
*/

        SELECT
                CAST(ROW_NUMBER() OVER(ORDER BY [ut].[MinLevel]) AS INT) AS [RangeId]
                ,[ut].[RangeName]
                ,[ut].[MinLevel]
                ,[ut].[MaxLevel]
        INTO
                [#ranges]
        FROM
        (
            SELECT
                    '<5k' AS [RangeName]
                    ,NULL AS  [MinLevel]
                    ,5000 AS  [MaxLevel]
            UNION
            SELECT
                    '5k-10k' AS [RangeName]
                    ,5000 AS     [MinLevel]
                    ,10000 AS    [MaxLevel]
            UNION
            SELECT
                    '10k-15k' AS [RangeName]
                    ,10000 AS     [MinLevel]
                    ,15000 AS     [MaxLevel]
            UNION
            SELECT
                    '15k-25k' AS [RangeName]
                    ,15000 AS     [MinLevel]
                    ,25000 AS     [MaxLevel]
            UNION
            SELECT
                    '25k-50k' AS [RangeName]
                    ,25000 AS     [MinLevel]
                    ,50000 AS     [MaxLevel]
            UNION
            SELECT
                    '50k-75k' AS [RangeName]
                    ,50000 AS     [MinLevel]
                    ,75000 AS     [MaxLevel]
            UNION
            SELECT
                    '75k-100k' AS [RangeName]
                    ,75000 AS      [MinLevel]
                    ,100000 AS     [MaxLevel]
            UNION
            SELECT
                    '100k-125k' AS [RangeName]
                    ,100000 AS      [MinLevel]
                    ,125000 AS      [MaxLevel]
            UNION
            SELECT
                    '100k-125k' AS [RangeName]
                    ,125000 AS      [MinLevel]
                    ,150000 AS      [MaxLevel]
            UNION
            SELECT
                    '150k-200k' AS [RangeName]
                    ,150000 AS      [MinLevel]
                    ,200000 AS      [MaxLevel]
            UNION
            SELECT
                    '>200k' AS [RangeName]
                    ,200000 AS  [MinLevel]
                    ,NULL AS    [MaxLevel]
        ) AS [ut]
    END

    SELECT
            [r].[RangeId]
            ,COUNT(*) AS                            [TotalRecordsCount]
            ,AVG([a].[OriginalPrincipalBalance]) AS [OriginalPrincipalBalance]
            ,AVG([a].[debttoincome]) AS             [debttoincome]
            ,AVG([a].[loantoincome]) AS             [loantoincome]
            ,AVG([a].[TotalIncome]) AS              [TotalIncome]
            ,AVG([a].[IndexedDebtToIncome]) AS      [IndexedDebtToIncome]
            ,AVG([a].[IndexedLoanToIncome]) AS      [IndexedLoanToIncome]
            ,AVG([a].[CurrentInterestRate]) AS      [CurrentInterestRate]
            ,AVG([a].[OriginalLtv]) AS              [OriginalLtv]
            ,AVG([a].[OriginalLtfv]) AS             [OriginalLtfv]
            ,AVG([a].[OriginalForeclosureValue]) AS [OriginalForeclosureValue]
            ,AVG([a].[IndexedLTFV]) AS              [IndexedLTFV]
    INTO
            [#aggregated]
    FROM [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}] AS [a]
            LEFT JOIN [#ranges] AS [r]
                ON([r].[MinLevel] IS NULL
                AND [a].[CurrentPrincipalBalance] < [r].[MaxLevel])
                OR ([r].[MinLevel] <= [a].[CurrentPrincipalBalance]
                    AND [a].[CurrentPrincipalBalance] < [r].[MaxLevel])
                OR ([r].[MinLevel] <= [a].[CurrentPrincipalBalance]
                    AND [r].[MaxLevel] IS NULL)
    WHERE [a].[CurrentPrincipalBalance] IS NOT NULL
    GROUP BY
                [r].[RangeId]


    SELECT
            [r].[RangeName]
            ,[a].[RangeId] AS [SortId]
            ,[a].[TotalRecordsCount]
            ,[a].[OriginalPrincipalBalance]
            ,[a].[debttoincome]
            ,[a].[loantoincome]
            ,[a].[TotalIncome]
            ,[a].[IndexedDebtToIncome]
            ,[a].[IndexedLoanToIncome]
            ,[a].[CurrentInterestRate]
            ,[a].[OriginalLtv]
            ,[a].[OriginalLtfv]
            ,[a].[OriginalForeclosureValue]
            ,[a].[IndexedLTFV]
    FROM [#aggregated] AS [a]
            INNER JOIN [#ranges] AS [r]
                ON [a].[RangeId] = [r].[RangeId]

    RETURN 0");
            #endregion select
        }

        public override void Down() {
            DropStoredProcedure(Constants.PROCEDURE_AGGREGATE_CONTRACTS);
            DropStoredProcedure(Constants.PROCEDURE_SELECT_AGGREGATED_CONTRACTS);
            DropTable(Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL);
        }
    }
}