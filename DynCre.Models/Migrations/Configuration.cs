using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    internal sealed class Configuration: DbMigrationsConfiguration<DynDbContext> {
        public Configuration() {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DynDbContext context) {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}