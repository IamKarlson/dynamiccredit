using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class DataTypes: DbMigration {
        public override void Up() {
            AlterColumn("dbo.Contracts", "MBorrowerId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Contracts", "ClassType", c => c.Int(nullable: false));
            AlterColumn("dbo.Contracts", "OriginalPrincipalBalance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Contracts", "LoanOriginationDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Contracts", "MaturityDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Contracts", "TermToMaturity", c => c.Int(nullable: false));
            AlterColumn("dbo.Contracts", "Guarantee", c => c.Int(nullable: false));
            AlterColumn("dbo.Contracts", "LoanTypeIndicator", c => c.Int(nullable: false));
            AlterColumn("dbo.Contracts", "MLoanId", c => c.Guid(nullable: false));
            AlterColumn("dbo.HistoryElements", "MLoanId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Incomes", "BorrowerBirthDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Incomes", "MLoanId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Mortgages", "MLoanId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Rates", "MLoanId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Securitizations", "MLoanId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Valuations", "MLoanId", c => c.Guid(nullable: false));
        }

        public override void Down() {
            AlterColumn("dbo.Valuations", "MLoanId", c => c.String());
            AlterColumn("dbo.Securitizations", "MLoanId", c => c.String());
            AlterColumn("dbo.Rates", "MLoanId", c => c.String());
            AlterColumn("dbo.Mortgages", "MLoanId", c => c.String());
            AlterColumn("dbo.Incomes", "MLoanId", c => c.String());
            AlterColumn("dbo.Incomes", "BorrowerBirthDate", c => c.String());
            AlterColumn("dbo.HistoryElements", "MLoanId", c => c.String());
            AlterColumn("dbo.Contracts", "MLoanId", c => c.String());
            AlterColumn("dbo.Contracts", "LoanTypeIndicator", c => c.String());
            AlterColumn("dbo.Contracts", "Guarantee", c => c.String());
            AlterColumn("dbo.Contracts", "TermToMaturity", c => c.String());
            AlterColumn("dbo.Contracts", "MaturityDate", c => c.String());
            AlterColumn("dbo.Contracts", "LoanOriginationDate", c => c.String());
            AlterColumn("dbo.Contracts", "OriginalPrincipalBalance", c => c.String());
            AlterColumn("dbo.Contracts", "ClassType", c => c.String());
            AlterColumn("dbo.Contracts", "MBorrowerId", c => c.String());
        }
    }
}