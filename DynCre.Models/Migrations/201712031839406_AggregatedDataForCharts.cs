using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class AggregatedDataForCharts: DbMigration {
        public override void Up() {
            CreateStoredProcedure(Constants.PROCEDURE_SELECT_AGGREGATED_CONTRACTS_WITH_YEAR,
                                  body: $@"

    IF(OBJECT_ID('tempdb..#aggregated') IS NOT NULL)
       DROP TABLE [#aggregated]

    IF(OBJECT_ID('tempdb..#years') IS NOT NULL)
       DROP TABLE [#years]

    DECLARE @StartYear INT

    DECLARE @EndYear INT

    SELECT
         @StartYear = MIN(DATEPART(YEAR,[a].[LoanOriginationDate]))
        ,@EndYear = MAX(DATEPART(YEAR,[a].[LoanOriginationDate]))
    FROM [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}] AS [a]


    ;WITH years
        AS (
        SELECT
             @StartYear AS [Year]
        UNION ALL
        SELECT
             [Year] + 1
        FROM [years]
        WHERE [Year] < @EndYear)
        SELECT
             [years].[Year]
        INTO
            [#years]
        FROM [years]

    SELECT
         [r].[Id] AS                            [RangeId]
        ,[r].[year] AS                          [LoanOriginationYear]
        ,COUNT(*) AS                            [TotalRecordsCount]
        ,AVG([a].[OriginalPrincipalBalance]) AS [OriginalPrincipalBalance]
        ,AVG([a].[debttoincome]) AS             [debttoincome]
        ,AVG([a].[loantoincome]) AS             [loantoincome]
        ,AVG([a].[TotalIncome]) AS              [TotalIncome]
        ,AVG([a].[IndexedDebtToIncome]) AS      [IndexedDebtToIncome]
        ,AVG([a].[IndexedLoanToIncome]) AS      [IndexedLoanToIncome]
        ,AVG([a].[CurrentInterestRate]) AS      [CurrentInterestRate]
        ,AVG([a].[OriginalLtv]) AS              [OriginalLtv]
        ,AVG([a].[OriginalLtfv]) AS             [OriginalLtfv]
        ,AVG([a].[OriginalForeclosureValue]) AS [OriginalForeclosureValue]
        ,AVG([a].[IndexedLTFV]) AS              [IndexedLTFV]
        ,AVG([a].[originalpropertyvalue]) AS    [OriginalPropertyValue]
    INTO
        [#aggregated]
    FROM
    (
       SELECT
            [r].[Id]
            ,[r].[Name]
            ,[r].[MinLevel]
            ,[r].[MaxLevel]
            ,[y].[Year]
       FROM [ranges] AS [r]
          ,[#years] AS [y]
    ) AS [r]
    LEFT JOIN [{Constants.TABLE_CONTRACTS_BY_CURRENT_PRINCIPAL}] AS [a]
       ON([r].[MinLevel] IS NULL
         AND ([a].[CurrentPrincipalBalance] < [r].[MaxLevel])
         OR ([r].[MinLevel] <= [a].[CurrentPrincipalBalance]
            AND [a].[CurrentPrincipalBalance] < [r].[MaxLevel])
         OR ([r].[MinLevel] <= [a].[CurrentPrincipalBalance]
            AND [r].[MaxLevel] IS NULL))
        AND DATEPART(YEAR,[a].[LoanOriginationDate]) = [r].[Year]
    GROUP BY
           [r].[id]
          ,[r].[year]

    SELECT
         [r].[Name] AS    [RangeName]
        ,[a].[RangeId] AS [SortId]
        ,[a].[LoanOriginationYear]
        ,[a].[TotalRecordsCount]
        ,[a].[OriginalPrincipalBalance]
        ,[a].[debttoincome]
        ,[a].[loantoincome]
        ,[a].[TotalIncome]
        ,[a].[IndexedDebtToIncome]
        ,[a].[IndexedLoanToIncome]
        ,[a].[CurrentInterestRate]
        ,[a].[OriginalLtv]
        ,[a].[OriginalLtfv]
        ,[a].[OriginalForeclosureValue]
        ,[a].[IndexedLTFV]
        ,[a].[OriginalPropertyValue]
    FROM [#aggregated] AS [a]
        INNER JOIN [Ranges] AS [r]
           ON [a].[RangeId] = [r].[Id]

");
        }

        public override void Down() {
        }
    }
}