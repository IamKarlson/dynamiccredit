using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class OrigionLoadSource: DbMigration {
        public override void Up() {
            AddColumn("dbo.Contracts", "OriginSource", c => c.String());
        }

        public override void Down() {
            DropColumn("dbo.Contracts", "OriginSource");
        }
    }
}