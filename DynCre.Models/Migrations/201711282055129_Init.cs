using System.Data.Entity.Migrations;

namespace DynCre.Models.Migrations {
    public partial class Init: DbMigration {
        public override void Up() {
            CreateTable("dbo.Contracts",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            MBorrowerId = c.String(),
                            ClassType = c.String(),
                            OriginalPrincipalBalance = c.String(),
                            LoanOriginationDate = c.String(),
                            MaturityDate = c.String(),
                            TermToMaturity = c.String(),
                            Guarantee = c.String(),
                            LoanTypeIndicator = c.String(),
                            Sector = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                            Income_Id = c.Int(),
                            Mortgage_Id = c.Int(),
                            Rate_Id = c.Int(),
                            Securitization_Id = c.Int(),
                            Valuation_Id = c.Int(),
                        }).PrimaryKey(t => t.Id).ForeignKey("dbo.Incomes", t => t.Income_Id).ForeignKey("dbo.Mortgages", t => t.Mortgage_Id)
                          .ForeignKey("dbo.Rates", t => t.Rate_Id).ForeignKey("dbo.Securitizations", t => t.Securitization_Id)
                          .ForeignKey("dbo.Valuations", t => t.Valuation_Id).Index(t => t.Income_Id).Index(t => t.Mortgage_Id)
                          .Index(t => t.Rate_Id).Index(t => t.Securitization_Id).Index(t => t.Valuation_Id);

            CreateTable("dbo.HistoryElements",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            ReportDate = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                            CurrentPrincipalBalance = c.String(),
                            CurrentLtv = c.String(),
                            LoanAge = c.String(),
                            CurrentPropertyVal = c.String(),
                            Savings = c.String(),
                            CurrentIndexedLtv = c.String(),
                            DelinquencyStatus = c.String(),
                            BorrowerExposure = c.String(),
                            NoBorrowers = c.String(),
                            CurrentRateType = c.String(),
                            CurrentInterestRate = c.String(),
                            CurrentDti = c.String(),
                            CurrentLtfv = c.String(),
                            Discriminator = c.String(nullable: false, maxLength: 128),
                            Contract_Id = c.Int(),
                        }).PrimaryKey(t => t.Id).ForeignKey("dbo.Contracts", t => t.Contract_Id).Index(t => t.Contract_Id);

            CreateTable("dbo.Incomes",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            Dti = c.String(),
                            Lti = c.String(),
                            SelfCertification = c.String(),
                            EmploymentStatus = c.String(),
                            TotalIncome = c.String(),
                            BorrowerBirthDate = c.String(),
                            IndexedDti = c.String(),
                            IndexedLti = c.String(),
                            IndexedTotalIncome = c.String(),
                            MaxBorrowerIncome = c.String(),
                            BorrowerTotalIncome = c.String(),
                            LivingExpenses = c.String(),
                            CurrentInterestShock = c.String(),
                            MonthlyNetIncome = c.String(),
                            MonthlyMortgageInterest = c.String(),
                            MonthlyMortgagePrincipal = c.String(),
                            MonthlyIncomeBuffer = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                        }).PrimaryKey(t => t.Id);

            CreateTable("dbo.Mortgages",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            BtlStatus = c.String(),
                            RepaymentType = c.String(),
                            SecondLien = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                        }).PrimaryKey(t => t.Id);

            CreateTable("dbo.Rates",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            ReversionaryDate = c.String(),
                            InterestStartDate = c.String(),
                            RemainingFixedRateTerm = c.String(),
                            PrincipalStartDate = c.String(),
                            OriginalFixedRateTerm = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                        }).PrimaryKey(t => t.Id);

            CreateTable("dbo.Securitizations",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            LifetimeBasePd = c.String(),
                            LifetimeBasePdOverride = c.String(),
                            MultipleNew = c.String(),
                            LifetimeBasePpOverride = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                        }).PrimaryKey(t => t.Id);

            CreateTable("dbo.Valuations",
                        c => new {
                            Id = c.Int(nullable: false, identity: true),
                            OriginalPropertyValue = c.String(),
                            PropertyValuationDate = c.String(),
                            OriginalLtv = c.String(),
                            OriginalLtfv = c.String(),
                            OriginalForeclosureValue = c.String(),
                            IndexedLtfv = c.String(),
                            CurrentForeclosureValue = c.String(),
                            CurrentPropertyValue = c.String(),
                            CurrentNetPropertyValue = c.String(),
                            ForeclosureResidualDebtEstimate = c.String(),
                            LoanLossEstimate = c.String(),
                            LoanPrice = c.String(),
                            MPoolId = c.String(),
                            MLoanId = c.String(),
                        }).PrimaryKey(t => t.Id);
        }

        public override void Down() {
            DropForeignKey("dbo.Contracts", "Valuation_Id", "dbo.Valuations");
            DropForeignKey("dbo.Contracts", "Securitization_Id", "dbo.Securitizations");
            DropForeignKey("dbo.Contracts", "Rate_Id", "dbo.Rates");
            DropForeignKey("dbo.Contracts", "Mortgage_Id", "dbo.Mortgages");
            DropForeignKey("dbo.Contracts", "Income_Id", "dbo.Incomes");
            DropForeignKey("dbo.HistoryElements", "Contract_Id", "dbo.Contracts");
            DropIndex("dbo.HistoryElements",
                      new[] {
                          "Contract_Id"
                      });
            DropIndex("dbo.Contracts",
                      new[] {
                          "Valuation_Id"
                      });
            DropIndex("dbo.Contracts",
                      new[] {
                          "Securitization_Id"
                      });
            DropIndex("dbo.Contracts",
                      new[] {
                          "Rate_Id"
                      });
            DropIndex("dbo.Contracts",
                      new[] {
                          "Mortgage_Id"
                      });
            DropIndex("dbo.Contracts",
                      new[] {
                          "Income_Id"
                      });
            DropTable("dbo.Valuations");
            DropTable("dbo.Securitizations");
            DropTable("dbo.Rates");
            DropTable("dbo.Mortgages");
            DropTable("dbo.Incomes");
            DropTable("dbo.HistoryElements");
            DropTable("dbo.Contracts");
        }
    }
}