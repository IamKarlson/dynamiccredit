﻿using System.Xml;
using System.Xml.Serialization;

namespace DynCre.Models.Serialization {
    [XmlRoot(ElementName = "root")]
    public class ContractLoad {
        [XmlElement(ElementName = "MASTER")]
        public Contract Contract { get; set; }

        [XmlAnyElement]
        public XmlElement[] Unsupported { get; set; }
    }
}