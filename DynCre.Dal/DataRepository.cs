﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

using DynCre.Models;
using DynCre.Models.DTO;

using NLog;

namespace DynCre.Dal {
    public class DataRepository: IDataRepository {
        private readonly DynDbContext db;

        private readonly Logger log = LogManager.GetCurrentClassLogger();

        public DataRepository() {
            log.Trace("ctor");
            db = new DynDbContext();
        }

        public async Task<List<AggregatedDataDto>> GetAggregatedDataForContractsByCurrentPrincipal() {
            log.Trace("start");
            try {
                var list = await db.Database.SqlQuery<AggregatedDataDto>(Constants.PROCEDURE_SELECT_AGGREGATED_CONTRACTS).ToListAsync();
                list = list.OrderBy(t => t.SortId).ToList();
                log.Trace("finish");
                return list;
            }
            catch(Exception ex) {
                log.Error(ex, "Cannot load data for the report");
                return null;
            }
        }

        public async Task<List<AggregatedChartDataDto>> GetAggregatedChartData() {
            log.Trace("start");
            try {
                DbRawSqlQuery<AggregatedChartDataDto> aggregatedChartDataDtos =
                    db.Database.SqlQuery<AggregatedChartDataDto>(Constants.PROCEDURE_SELECT_AGGREGATED_CONTRACTS_WITH_YEAR);
                var list = await aggregatedChartDataDtos.ToListAsync();
                list = list.OrderBy(t => t.SortId).ToList();
                log.Trace("finish");
                return list;
            }
            catch(Exception ex) {
                log.Error(ex, "Cannot load data for the chart");
                return null;
            }
        }

        public async Task<bool> PrepareData() {
            log.Trace("start");
            try {
                await db.Database.ExecuteSqlCommandAsync(Constants.PROCEDURE_AGGREGATE_CONTRACTS);
                await db.Database.ExecuteSqlCommandAsync(Constants.PROCEDURE_SET_UP_BUCKETS);
                log.Trace("finish");
                return true;
            }
            catch(Exception ex) {
                log.Error(ex, "Cannot prepare data for the report");
                return false;
            }
        }
    }
}