﻿using System.Collections.Generic;
using System.Threading.Tasks;

using DynCre.Models.DTO;

namespace DynCre.Dal {
    public interface IDataRepository {
        Task<List<AggregatedDataDto>> GetAggregatedDataForContractsByCurrentPrincipal();

        Task<List<AggregatedChartDataDto>> GetAggregatedChartData();

        Task<bool> PrepareData();
    }
}