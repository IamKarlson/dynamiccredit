﻿using System;

using Quartz;
using Quartz.Simpl;
using Quartz.Spi;

using SimpleInjector;

namespace DynCre.Jobs {
    public class InjectedJobFactory: SimpleJobFactory {
        private readonly Container container;

        public InjectedJobFactory(Container container) {
            this.container = container;
        }

        public override IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler) {
            try {
                // this will inject dependencies that the job requires
                return (IJob) container.GetInstance(bundle.JobDetail.JobType);
            }
            catch(Exception e) {
                throw new SchedulerException(
                    $"Problem while instantiating job '{bundle.JobDetail.Key}' from the {nameof(InjectedJobFactory)}.",
                    e);
            }
        }
    }
}