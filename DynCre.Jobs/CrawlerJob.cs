﻿using DynCre.Services;

using NLog;

using Quartz;

namespace DynCre.Jobs {
    [DisallowConcurrentExecution]
    public class CrawlerJob: IJob {
        private readonly IStorageLoader loader;

        private readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly IFileSearcher searcher;

        public CrawlerJob(IFileSearcher searcher, IStorageLoader loader) {
            this.searcher = searcher;
            this.loader = loader;
        }

        public void Execute(IJobExecutionContext context) {
            log.Trace("job start");
            var files = searcher.GetFiles();

            bool result = loader.LoadFiles(files).Result;
            if(result) { log.Debug($"Files are loaded from succesfully."); }
            else { log.Debug($"Files are not loaded. Reasons are mentioned above in the log."); }
            log.Trace("job end");
        }
    }
}