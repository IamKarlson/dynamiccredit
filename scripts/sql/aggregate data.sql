CREATE PROCEDURE [GetAggregatedData]
AS

/* 
Used for showing data on the page
*/

BEGIN
/* 
Used for showing data on the page
*/
    IF(OBJECT_ID('tempdb..#aggregated') IS NOT NULL)
        DROP TABLE [#aggregated]

    IF(OBJECT_ID('tempdb..#ranges') IS NOT NULL)
        DROP TABLE [#ranges]

    BEGIN

/*
Generating ranges
*/

        SELECT
                ROW_NUMBER() OVER(ORDER BY [ut].[MinLevel]) AS [RangeId]
                ,[ut].[RangeName]
                ,[ut].[MinLevel]
                ,[ut].[MaxLevel]
        INTO
                [#ranges]
        FROM
        (
            SELECT
                    '<5k' AS [RangeName]
                    ,NULL AS  [MinLevel]
                    ,5000 AS  [MaxLevel]
            UNION
            SELECT
                    '5k-10k' AS [RangeName]
                    ,5000 AS     [MinLevel]
                    ,10000 AS    [MaxLevel]
            UNION
            SELECT
                    '10k-15k' AS [RangeName]
                    ,10000 AS     [MinLevel]
                    ,15000 AS     [MaxLevel]
            UNION
            SELECT
                    '15k-25k' AS [RangeName]
                    ,15000 AS     [MinLevel]
                    ,25000 AS     [MaxLevel]
            UNION
            SELECT
                    '25k-50k' AS [RangeName]
                    ,25000 AS     [MinLevel]
                    ,50000 AS     [MaxLevel]
            UNION
            SELECT
                    '50k-75k' AS [RangeName]
                    ,50000 AS     [MinLevel]
                    ,75000 AS     [MaxLevel]
            UNION
            SELECT
                    '75k-100k' AS [RangeName]
                    ,75000 AS      [MinLevel]
                    ,100000 AS     [MaxLevel]
            UNION
            SELECT
                    '100k-125k' AS [RangeName]
                    ,100000 AS      [MinLevel]
                    ,125000 AS      [MaxLevel]
            UNION
            SELECT
                    '100k-125k' AS [RangeName]
                    ,125000 AS      [MinLevel]
                    ,150000 AS      [MaxLevel]
            UNION
            SELECT
                    '150k-200k' AS [RangeName]
                    ,150000 AS      [MinLevel]
                    ,200000 AS      [MaxLevel]
            UNION
            SELECT
                    '>200k' AS [RangeName]
                    ,200000 AS  [MinLevel]
                    ,NULL AS    [MaxLevel]
        ) AS [ut]
    END

    SELECT
            [r].[RangeId]
            ,COUNT(*) AS                            [TotalRecordsCount]
            ,AVG([a].[OriginalPrincipalBalance]) AS [OriginalPrincipalBalance]
            ,AVG([a].[debttoincome]) AS             [debttoincome]
            ,AVG([a].[loantoincome]) AS             [loantoincome]
            ,AVG([a].[TotalIncome]) AS              [TotalIncome]
            ,AVG([a].[IndexedDebtToIncome]) AS      [IndexedDebtToIncome]
            ,AVG([a].[IndexedLoanToIncome]) AS      [IndexedLoanToIncome]
            ,AVG([a].[CurrentInterestRate]) AS      [CurrentInterestRate]
            ,AVG([a].[OriginalLtv]) AS              [OriginalLtv]
            ,AVG([a].[OriginalLtfv]) AS             [OriginalLtfv]
            ,AVG([a].[OriginalForeclosureValue]) AS [OriginalForeclosureValue]
            ,AVG([a].[IndexedLTFV]) AS              [IndexedLTFV]
    INTO
            [#aggregated]
    FROM [AggregatedContractsByCurrentPrincipal] AS [a]
            LEFT JOIN [#ranges] AS [r]
                ON([r].[MinLevel] IS NULL
                AND [a].[CurrentPrincipalBalance] < [r].[MaxLevel])
                OR ([r].[MinLevel] <= [a].[CurrentPrincipalBalance]
                    AND [a].[CurrentPrincipalBalance] < [r].[MaxLevel])
                OR ([r].[MinLevel] <= [a].[CurrentPrincipalBalance]
                    AND [r].[MaxLevel] IS NULL)
    WHERE [a].[CurrentPrincipalBalance] IS NOT NULL
    GROUP BY
                [r].[RangeId]


    SELECT
            [r].[RangeName]
            ,[a].[RangeId] AS [SortId]
            ,[a].[TotalRecordsCount]
            ,[a].[OriginalPrincipalBalance]
            ,[a].[debttoincome]
            ,[a].[loantoincome]
            ,[a].[TotalIncome]
            ,[a].[IndexedDebtToIncome]
            ,[a].[IndexedLoanToIncome]
            ,[a].[CurrentInterestRate]
            ,[a].[OriginalLtv]
            ,[a].[OriginalLtfv]
            ,[a].[OriginalForeclosureValue]
            ,[a].[IndexedLTFV]
    FROM [#aggregated] AS [a]
            INNER JOIN [#ranges] AS [r]
                ON [a].[RangeId] = [r].[RangeId]

    RETURN 0
END