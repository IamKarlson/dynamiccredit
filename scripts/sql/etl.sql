CREATE PROCEDURE [AggregateContracts]
/*
* Procedure is used for merging data for the report
*/
AS
BEGIN	

/*
commands for debug

SELECT * FROM AggregatedContractsByCurrentPrincipal
DROP TABLE [AggregatedContractsByCurrentPrincipal]
*/

    IF(OBJECT_ID('AggregatedContractsByCurrentPrincipal') IS NULL)
        BEGIN
            CREATE TABLE [dbo].[AggregatedContractsByCurrentPrincipal]
            (
                            [ContractId]               INT,
                            [CurrentPrincipalBalance]  DECIMAL(18, 2) NULL,
                            [OriginalPrincipalBalance] DECIMAL(18, 2) NULL,
                            [debttoincome]             DECIMAL(18, 2) NULL,
                            [loantoincome]             DECIMAL(18, 2) NULL,
                            [TotalIncome]              DECIMAL(18, 2) NULL,
                            [IndexedDebtToIncome]      DECIMAL(18, 2) NULL,
                            [IndexedLoanToIncome]      DECIMAL(18, 2) NULL,
                            [CurrentInterestRate]      DECIMAL(18, 2) NULL,
                            [OriginalLtv]              DECIMAL(18, 2) NULL,
                            [OriginalLtfv]             DECIMAL(18, 2) NULL,
                            [OriginalForeclosureValue] DECIMAL(18, 2) NULL,
                            [IndexedLTFV]              DECIMAL(18, 2) NULL
            )
            ON [PRIMARY]
        END
    DECLARE @Unmerged INT
    SELECT
            @unmerged = COUNT(*)
    FROM [Contracts]
    WHERE [IsAggergated] = 0

    IF(@Unmerged = 0)
    BEGIN
        RETURN 0
    END


    BEGIN TRAN [TransformData]

    BEGIN TRY

        INSERT INTO [AggregatedContractsByCurrentPrincipal]
                SELECT
                        [c].[Id] AS [ContractId],
                        [h].[CurrentPrincipalBalance],
                        [c].[OriginalPrincipalBalance],
                        [i].[debttoincome],
                        [i].[loantoincome],
                        [i].[TotalIncome],
                        [i].[IndexedDebtToIncome],
                        [i].[IndexedLoanToIncome],
                        [his].[CurrentInterestRate],
                        [v].[OriginalLtv],
                        [v].[OriginalLtfv],
                        [v].[OriginalForeclosureValue],
                        [v].[IndexedLTFV]
                FROM [dbo].[Contracts] AS [c]
                    LEFT JOIN [dbo].[Incomes] AS [i]
                        ON [i].[id] = [c].[Income_Id]
                    LEFT JOIN [dbo].[HistoryElements] AS [he]
                        ON [c].[Id] = [he].[Contract_Id]
                    LEFT JOIN [dbo].[History] AS [h]
                        ON [he].[Id] = [h].[Id]
                    LEFT JOIN [dbo].[HistoryRate] AS [his]
                        ON [he].[Id] = [his].[Id]
                    LEFT JOIN [dbo].[Valuations] AS [v]
                        ON [v].[id] = [c].[Valuation_Id]

        UPDATE [c]
            SET
                [IsAggergated] = 1
        FROM [dbo].[Contracts] AS [c]
                INNER JOIN [dbo].[AggregatedContractsByCurrentPrincipal] AS [cp]
                    ON [cp].[ContractId] = [c].[id]
    END TRY
    BEGIN CATCH
        ROLLBACK TRAN [TransformData]
        RETURN
    END CATCH

    COMMIT TRAN [TransformData]
END